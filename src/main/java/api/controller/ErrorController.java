package api.controller;

import api.response.ResponseHandler;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;

@Controller
public class ErrorController implements org.springframework.boot.web.servlet.error.ErrorController {
    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    @RequestMapping("/error")
    public ResponseEntity<?> handleError() {
        return ResponseHandler.generateBadResponse("Item not found", HttpStatus.NOT_FOUND);
    }

    @Override
    public String getErrorPath() {
        return "/error";
    }
}
