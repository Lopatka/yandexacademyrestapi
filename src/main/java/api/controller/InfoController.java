package api.controller;


import api.entity.Item;
import io.github.resilience4j.ratelimiter.RequestNotPermitted;
import io.github.resilience4j.ratelimiter.annotation.RateLimiter;
import lombok.extern.slf4j.Slf4j;
import api.Constants;
import api.response.ResponseHandler;
import api.service.ItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.List;

@Slf4j
@Validated
@RequestMapping(produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
@RateLimiter(name = "info", fallbackMethod = "rateLimiterFallback")
@RestController
public class InfoController {
    @Autowired
    private ItemService itemService;

    @RequestMapping(value = "/nodes/{id}", method = RequestMethod.GET)
    public ResponseEntity<?> getItemById(@Valid @PathVariable("id") @NotBlank String id) {
        if (id == null) {
            return ResponseHandler.generateBadResponse("Validation Failed", HttpStatus.BAD_REQUEST);
        }

        Item item = itemService.getItemFullInfo(id);

        if (item == null) {
            return ResponseHandler.generateBadResponse("Item not found", HttpStatus.NOT_FOUND);
        }
        return ResponseHandler.generateResponse(item, HttpStatus.OK);
    }

    @RequestMapping(value = "/updates", method = RequestMethod.GET)
    public ResponseEntity<?> getUpdates(@RequestParam("date") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) @NotNull LocalDateTime date) {
        List<Item> itemList = itemService.getUpdatesOfDay(date);
        return ResponseHandler.generateResponse(itemList, HttpStatus.OK);
    }

    @RequestMapping(value = "/node/{id}/history", method = RequestMethod.GET)
    public ResponseEntity<?> getItemHistoryById(@Valid @PathVariable("id") @NotBlank String id,
                                            @RequestParam(value = "dateStart", required = false)
                                            @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime dateStart,
                                            @RequestParam(value = "dateEnd", required = false)
                                            @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime dateEnd) {
        if (dateStart == null) {
            dateStart = LocalDateTime.parse(Constants.MIN_TIMESTAMP);
        }
        if (dateEnd == null) {
            dateEnd = LocalDateTime.parse(Constants.MAX_TIMESTAMP);
        }
        List<Item> itemList = itemService.getItemHistory(id, dateStart.toString(), dateEnd.toString());
        return ResponseHandler.generateResponse(itemList, HttpStatus.OK);
    }

    public ResponseEntity<?> rateLimiterFallback(RequestNotPermitted e) {
        return ResponseHandler.generateBadResponse("Too many requests", HttpStatus.TOO_MANY_REQUESTS);
    }
}
