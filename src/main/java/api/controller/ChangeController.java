package api.controller;

import api.entity.Item;
import api.service.ItemService;
import lombok.extern.slf4j.Slf4j;
import api.dto.ImportRequest;
import api.response.ResponseHandler;
import api.service.ImportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Slf4j
@Validated
@RequestMapping(produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
@RestController
public class ChangeController {

    @Autowired
    private ItemService itemService;
    @Autowired
    private ImportService importService;


    @RequestMapping(value = "/imports", method = RequestMethod.POST)
    public ResponseEntity<?> saveItems(@Valid @RequestBody(required = false) ImportRequest importRequest) {
        if (!importService.validateAllParent(importRequest, this.itemService)) {
            return ResponseHandler.generateBadResponse("Validation Failed", HttpStatus.BAD_REQUEST);
        }
        if (importService.saveList(importRequest)) {
            return ResponseHandler.generateResponse(HttpStatus.OK);
        } else {
            return ResponseHandler.generateBadResponse("Validation Failed", HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteItem(@PathVariable("id") @NotBlank String id, @RequestParam("date") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) @NotNull LocalDateTime date) {
        Item item = itemService.getById(id);
        if (item == null) {
            return ResponseHandler.generateBadResponse("Item not found", HttpStatus.NOT_FOUND);
        }
        itemService.deleteHierarchyById(id);
        return ResponseHandler.generateResponse(HttpStatus.OK);
    }
}
