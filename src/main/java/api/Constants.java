package api;

public class Constants {
    public static final String MIN_TIMESTAMP = "1970-01-01T03:00:00";
    public static final String MAX_TIMESTAMP = "9999-12-31T23:59:58";
}
