package api.service;

import api.entity.Item;
import api.repository.ItemRepository;
import lombok.extern.slf4j.Slf4j;
import api.dto.ImportRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@Slf4j
@Service
public class ImportServiceImpl implements ImportService {

    @Autowired
    ItemRepository itemRepository;

    @Override
    public boolean saveList(ImportRequest importRequest) {
        if (importRequest.getItems() == null) {
            return false;
        }
        for (Item item : importRequest.getItems()) {
            item.setDate(importRequest.getUpdateDate());
        }
        itemRepository.saveAll(importRequest.getItems());
        log.info("IN ImportServiceImpl saveArray {}", importRequest);
        return true;
    }

    @Override
    public boolean validateAllParent(ImportRequest importRequest, ItemService itemService) {
        Set<String> itemNoParentsRequestList = new HashSet<>();
        HashMap<String, Item> itemMap = new HashMap<>();
        Set<Item> dbItems;

        for (Item item : importRequest.getItems()) {
            itemMap.put(item.getId(), item);
        }

        if (!validateRequestItemsParent(itemMap, itemNoParentsRequestList)) {
            return false;
        }

        if (itemNoParentsRequestList.isEmpty()){
            itemNoParentsRequestList.add("NULL");
            dbItems = findDistinctIdIn(itemNoParentsRequestList);
            itemNoParentsRequestList.remove("NULL");
        } else {
            dbItems = findDistinctIdIn(itemNoParentsRequestList);
        }

        if (dbItems.size() != itemNoParentsRequestList.size()) {
            return false;
        }

        for (Item item : dbItems) {
            if (!item.getType().equals(Item.ItemType.FOLDER.value())) {
                return false;
            }
        }
        return true;
    }

    @Override
    public Set<Item> findDistinctIdIn(Set<String> ids) {
        return itemRepository.findDistinctIdIn(ids);
    }

    private boolean validateRequestItemsParent(Map<String, Item> itemMap, Set<String> result) {
        for (Map.Entry<String, Item> element : itemMap.entrySet()) {
            if (!itemMap.containsKey(element.getValue().getParentId())) {
                if (element.getValue().getParentId() != null) {
                    result.add(element.getValue().getParentId());
                }
            } else if (!(itemMap.get(element.getValue().getParentId())).getType().equals(Item.ItemType.FOLDER.value())) {
                log.info("IN ImportServiceImpl checkRequestItemsParent {}", element.getValue());
                return false;
            }
        }
        return true;
    }
}
