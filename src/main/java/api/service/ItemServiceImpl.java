package api.service;

import api.entity.Item;
import lombok.extern.slf4j.Slf4j;
import api.repository.ItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.*;


@Slf4j
@Service
public class ItemServiceImpl implements ItemService {

    @Autowired
    ItemRepository itemRepository;

    @Override
    public Item getById(String id) {
        log.info("IN ItemServiceImpl getById {}", id);
        return itemRepository.getById(id);
    }

    private List<Item> getHierarchyById(String id) {
        log.info("IN ItemServiceImpl getHierarchyOfItem {}", id);
        return itemRepository.getHierarchyById(id);
    }

    @Override
    public void deleteHierarchyById(String id) {
        log.info("IN ItemServiceImpl deleteHierarchyById {}", id);
        itemRepository.deleteHierarchyById(id);
    }

    @Override
    public Item getItemFullInfo(String id) {
        ArrayList<Item> itemList = (ArrayList<Item>) getHierarchyById(id);
        Map<String, Item> itemMap = new HashMap<>();
        Item resultItem = null;
        Item current;

        Collections.reverse(itemList);

        for (Item item : itemList) {
            itemMap.put(item.getId(), item);
            if (item.getType().equals(Item.ItemType.FOLDER.value())) {
                item.setChildren(new ArrayList<>());
            }
        }

        for (Map.Entry<String, Item> element : itemMap.entrySet()) {
            if (element.getKey().equals(id)) {
                resultItem = element.getValue();
            } else {
                current = itemMap.get(element.getValue().getParentId());
                current.addChildren(element.getValue());
            }
        }

        for (Item item : itemList) {
            current = itemMap.get(item.getParentId());
            if (current != null) {
                if (current.getSize() == null) {
                    current.setSize(0L);
                }
                if (item.getSize() == null) {
                    item.setSize(0L);
                }
                current.setSize(current.getSize() + item.getSize());
            }
        }
        return resultItem;
    }

    @Override
    public List<Item> getUpdatesOfDay(LocalDateTime date) {
        return itemRepository.getUpdatesOfDay(date);
    }

    @Override
    public List<Item> getItemHistory(String id, String dateStart, String dateEnd) {
        return itemRepository.getItemHistory(id, dateStart, dateEnd);
    }
}
