package api.service;

import api.dto.ImportRequest;
import api.entity.Item;

import java.util.Set;

public interface ImportService {
    boolean saveList(ImportRequest importRequest);

    boolean validateAllParent(ImportRequest importRequest, ItemService itemService);

    Set<Item> findDistinctIdIn(Set<String> ids);
}
