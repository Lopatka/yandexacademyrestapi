package api.service;

import api.entity.Item;

import java.time.LocalDateTime;
import java.util.List;


public interface ItemService {
    Item getById(String id);
    void deleteHierarchyById(String id);
    Item getItemFullInfo(String id);
    List<Item> getUpdatesOfDay(LocalDateTime date);
    List<Item> getItemHistory(String id, String dateStart, String dateEnd);
}
