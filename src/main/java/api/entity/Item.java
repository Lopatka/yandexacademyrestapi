package api.entity;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.*;
import api.validator.ItemSize;
import api.validator.ItemUrl;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "items_history")
@Getter
@Setter
@ToString
@ItemUrl
@ItemSize
@IdClass(ItemPrimaryKeys.class)
@AllArgsConstructor()
@NoArgsConstructor
public class Item {


    @NotNull
    @Id
    @Column(name = "id")
    private String id;

    @Column(name = "url")
    private String url;

    @Column(name = "parent_id")
    private String parentId;

    @Id
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    @Column(name = "update_date")
    private LocalDateTime date;

    @Column(name = "size")
    private Long size;

    @Column(name = "type")
    private String type;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    transient private List<Item> children;


    public void addChildren(Item item) {
        if (children == null) {
            children = new ArrayList<>();
        }
        children.add(item);
    }

    public enum ItemType {
        FILE("FILE"),
        FOLDER("FOLDER");

        private final String value;

        ItemType(String value) {
            this.value = value;
        }

        public String value() {
            return this.value;
        }
    }
}
class ItemPrimaryKeys implements Serializable {
    private String id;
    private LocalDateTime date;

    @Override
    public int hashCode() {
        return Objects.hash( id, date );
    }

    @Override
    public boolean equals(Object obj) {
        if ( this == obj ) {
            return true;
        }
        if ( obj == null || getClass() != obj.getClass() ) {
            return false;
        }
        ItemPrimaryKeys pk = (ItemPrimaryKeys) obj;
        return Objects.equals( id, pk.id ) &&
                Objects.equals( date, pk.date );
    }
}
