package api.validator;

import api.entity.Item;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class ItemUrlValidator implements ConstraintValidator<ItemUrl, Item> {

    @Override
    public boolean isValid(Item item, ConstraintValidatorContext context) {
        if (item.getUrl() == null) {
            return true;
        }
        if (item.getType().equals(Item.ItemType.FILE.value())) {
            if (item.getUrl().length() > 255) {
                return false;
            }
        }
        if (item.getType().equals(Item.ItemType.FOLDER.value())) {
            return item.getUrl() == null;
        }
        return true;
    }
}
