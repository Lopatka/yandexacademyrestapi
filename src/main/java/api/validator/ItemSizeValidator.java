package api.validator;

import api.entity.Item;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class ItemSizeValidator implements ConstraintValidator<ItemSize, Item> {

    @Override
    public boolean isValid(Item item, ConstraintValidatorContext context) {
        if (item.getType().equals(Item.ItemType.FILE.value())) {
            if (item.getSize() == null) {
                return false;
            }
            if (item.getSize() <= 0) {
                return false;
            }
        }
        if (item.getType().equals(Item.ItemType.FOLDER.value())) {
            return item.getSize() == null;
        }
        return true;
    }
}
