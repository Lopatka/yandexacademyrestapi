package api.repository;

import api.entity.Item;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

@Repository
public interface ItemRepository extends JpaRepository<Item, String> {

    @Query(value = "SELECT DISTINCT(id) , url, parent_id, size, update_date, type \n" +
            "FROM items_history\n" +
            "WHERE (id = ?1) AND (update_date = (SELECT MAX(update_date)\n" +
            "        FROM items_history WHERE  id = ?1))", nativeQuery = true)
    Item getById(String id);
    @Query(value = "SELECT DISTINCT(id) , url, parent_id, size, MAX(update_date) OVER (partition BY id) as update_date, type \n" +
            "FROM items_history\n" +
            "WHERE id IN (:ids)", nativeQuery = true)
    Set<Item> findDistinctIdIn(@Param("ids") Set<String> ids);

    @Query(value = "SELECT id , url, parent_id, size, update_date,type \n" +
            "FROM items_history\n" +
            "WHERE id = ?1 AND update_date  >= CAST(?2 AS TIMESTAMP WITH TIME ZONE) AND update_date  < CAST(?3 AS TIMESTAMP WITH TIME ZONE)", nativeQuery = true)
    List<Item> getItemHistory(String id, String dateStart, String dateEnd);

    @Query(value = "SELECT t.id , t.url, t.parent_id, t.size, t.update_date, t.type\n" +
            "FROM items_history t\n" +
            "INNER JOIN (\n" +
            "SELECT id , MAX (update_date) as update_date\n" +
            "FROM items_history\n" +
            "WHERE \t\n" +
            "(update_date  >= CAST(?1 AS TIMESTAMP) - INTERVAL '1 DAY')\n" +
            "AND (update_date <= CAST(?1 AS TIMESTAMP))\n" +
            "GROUP BY id\n" +
            ") p ON t.id = p.id AND t.update_date = p.update_date", nativeQuery = true)
    List<Item> getUpdatesOfDay(LocalDateTime date);

    @Query(value = "WITH RECURSIVE dep AS(\n" +
            "SELECT  id, url, parent_id, size, update_date, type\n" +
            "FROM public.items_history\n" +
            "WHERE id = ?1\n" +
            "UNION ALL\n" +
            "SELECT  c.id, c.url, c.parent_id, c.size ,c.update_date, c.type\n" +
            "\n" +
            "FROM dep p, public.items_history c\n" +
            "WHERE c.parent_id = p.id\n" +
            ")\n" +
            "SELECT DISTINCT(id) , url, parent_id, size, MAX(update_date) OVER (partition BY id) as update_date, type FROM dep\n" +
            "\n", nativeQuery = true)
    List<Item> getHierarchyById(String id);

    @Transactional
    @Modifying
    @Query(value = "WITH RECURSIVE dep AS(\n" +
            "SELECT  id, url, parent_id, size, update_date, type\n" +
            "FROM public.items_history\n" +
            "WHERE id = ?1\n" +
            "UNION ALL\n" +
            "SELECT  c.id, c.url, c.parent_id, c.size ,c.update_date, c.type\n" +
            "\n" +
            "FROM dep p, public.items_history c\n" +
            "WHERE c.parent_id = p.id\n" +
            ")\n" +
            "DELETE FROM public.items_history\n" +
            "WHERE id IN (SELECT id FROM dep)", nativeQuery = true)
    void deleteHierarchyById(String id);
}
