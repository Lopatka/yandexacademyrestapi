CREATE TABLE IF NOT EXISTS items_history(
    id          VARCHAR NOT NULL,
    url         VARCHAR(255),
    parent_id   VARCHAR,
    size        BIGINT,
    update_date TIMESTAMP,
    type        VARCHAR CHECK(type = 'FILE' OR type = 'FOLDER'),
    PRIMARY KEY(id, update_date)
);